# [T3chFlicks - Smart Buoy](https://t3chflicks.com/shop/kit/smart-buoy)
## Server

This folder contains the code for the running the server for the
Smart Buoy Dashboard.

This code should be run on a Raspberry Pi, which has Python3 and mongoDB installed.

It uses Flask as a web server and communicates with the radio module (NRF24) and a mongo database.
Flask-socketio is used for communication with the frontend via websockets.

To run the server:
1. `pipenv install`
2. `pipenv shell`
3. `python app.py`

Create a mongoDB with DB name `buoyDB` and collection name `buoyMeasurements`.

![home_screen](./ss_home.png)

![trends_screen](./ss_trends.png)

If you want to play around with the dashboard without a Smart Buoy setup then just run `python fake_data_app.py`, and you should be able to see the data coming in.

Also for development purposes there is a `populate_db.py` script which does what is says on the tin.

If you just want to see the data coming from the Buoy to the Raspberry Pi via radio then you can use the `listen_to_radio.py` script.


### Step By Step Instructions
* Put a raspian image from [here](https://www.raspberrypi.org/downloads/) onto your pi sd card using [etcher](https://www.balena.io/etcher/).
* Plug in the nRF24L01 before turning on as in the diagram in the electronics folder.
* SSH into your pi (for the rest of the instructions stay in a terminal) and set up as usual [tutorial](https://www.youtube.com/watch?v=wvxCNQ5AYPg).
* Install python3 - [tutorial](https://installvirtual.com/install-python-3-7-on-raspberry-pi/).
* Install mongoDB - [tutorial](https://andyfelong.com/2019/01/mongodb-3-2-64-bit-running-on-raspberry-pi-3-with-caveats/).
* Create a DB in mongo called 'buoyDB' and a collection called 'buoyMeasurements'.
* Install Pipenv [tutoria](https://realpython.com/pipenv-guide/)
* Downlaod all the code and put into a folder, go into that folder.
* Run the aformentioned commands:
1. `pipenv install`
2. `pipenv shell`
3. `python app.py`